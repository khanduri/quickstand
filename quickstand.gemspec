# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "quickstand"
  spec.version       = "0.1.15"
  spec.authors       = ["Prashant Khanduri"]
  spec.email         = ["prashant.khanduri@gmail.com"]

  spec.summary       = "Blog theme."
  spec.homepage      = "https://bitbucket.org/khanduri/quickstand"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"
  spec.add_runtime_dependency "jekyll-paginate", "~> 1.1"
  # spec.add_runtime_dependency "jekyll-feed", "~> 0.10"
  # spec.add_runtime_dependency "jekyll-seo-tag", "~> 2.5"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
